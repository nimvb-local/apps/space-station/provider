package com.nimvb.app.spacestation.provider.service.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimvb.app.spacestation.provider.model.Event;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ReactiveEventProvider {

    private final WebClient webClient;
    private final ObjectMapper mapper;


    public Flux<Mono<JsonNode>> streamOf() {
        return Flux
                .interval(Duration.ofSeconds(2))
                .handle((aLong, sink) -> {
                    final Mono<JsonNode> request = webClient
                            .get()
                            .uri("/iss-now.json")
                            .retrieve()
                            .bodyToMono(String.class)
                            .retryWhen(Retry.fixedDelay(2L, Duration.ofMillis(1000)))
                            .onErrorReturn(mapper.createObjectNode().put("success", false).put("message", "failed").toString())
                            .timeout(Duration.ofMillis(5000))
                            .onErrorReturn(mapper.createObjectNode().put("success", false).put("message", "timeout").toString())
                            .map(response -> {
                                try {
                                    JsonNode result = mapper.readValue(response, JsonNode.class);
                                    if(!result.has("success")) {
                                        if (result.isObject()) {
                                            ((ObjectNode) result).put("success", true);
                                        }
                                    }
                                    return result;
                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                }
                                return mapper.createObjectNode();
                            })
                            .map(jsonNode -> {
                                return new Event(Instant.now().toEpochMilli(), jsonNode,UUID.randomUUID().toString());
                            })
                            .map(event -> {
                                try {
                                    return mapper.readValue(mapper.writeValueAsString(event),JsonNode.class);
                                } catch (JsonProcessingException e) {
                                    throw new RuntimeException();
                                }
                            })
                            .onErrorReturn(mapper.createObjectNode());

                    sink.next(request);
                })
                .map(mono -> (Mono<JsonNode>)mono)
                //.map(mono -> ((JsonNode) mono.share().block()))
                .onErrorReturn(Mono.fromSupplier(mapper::createObjectNode));
    }
}
