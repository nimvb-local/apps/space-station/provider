package com.nimvb.app.spacestation.provider.service.websocket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ReactiveWebsocketHandler implements WebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(ReactiveWebsocketHandler.class);
    private final ReactiveEventProvider provider;

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        final Flux<WebSocketMessage> stream = session
                .receive()
                .handle((message, sink) -> {
                    if (message.getPayloadAsText().equalsIgnoreCase("start")) {
                        sink.next(message);
                    } else if (message.getPayloadAsText().equalsIgnoreCase("stop")) {
                        sink.complete();
                    }
                })
                .switchMap(webSocketMessage -> {
                    return provider
                            .streamOf()
                            .flatMap(jsonNodeMono -> jsonNodeMono.map(jsonNode -> {
                                return jsonNode;
                            }))
                            .map(jsonNode -> {
                                ObjectMapper mapper = new ObjectMapper();
                                try {
                                    return session.textMessage(mapper.writeValueAsString(jsonNode));
                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                }
                                throw new RuntimeException();
                            });
                })
                .doOnCancel(() -> {
                    System.out.println("canceled");
                })
                .log();
        return session.send(stream);

//        return session
//                .send(provider.streamOf().map(session::textMessage))
//                .and(session
//                        .receive()
//                        .log(Loggers.getLogger(ReactiveWebsocketHandler.class))
//                        .map(WebSocketMessage::getPayloadAsText)
//                        .log(Loggers.getLogger(ReactiveWebsocketHandler.class)));
    }
}
