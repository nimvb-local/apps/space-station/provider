package com.nimvb.app.spacestation.provider.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.WebSocketService;
import org.springframework.web.reactive.socket.server.support.HandshakeWebSocketService;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;


import java.util.Map;

@Configuration
public class WebsocketConfiguration {

    @Bean
    public HandlerMapping webSocketRouter(WebSocketHandler handler) {
        SimpleUrlHandlerMapping router = new SimpleUrlHandlerMapping();
        router.setOrder(10);
        final Map<String, WebSocketHandler> handlerMap = Map.of("/locations", handler);
        router.setUrlMap(handlerMap);
        return router;
    }

    @Bean
    public WebSocketHandlerAdapter webSocketHandlerAdapter(WebSocketService webSocketService) {
        return new WebSocketHandlerAdapter(webSocketService);
    }

    @Bean
    public WebSocketService webSocketService(){
        return new HandshakeWebSocketService();
    }
}
