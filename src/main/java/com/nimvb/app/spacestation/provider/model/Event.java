package com.nimvb.app.spacestation.provider.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Value;

@Value
public class Event {
    @JsonProperty("response_timestamp")
    long timestamp;
    Object data;
    String id;
}
